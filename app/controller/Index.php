<?php
namespace app\controller;

use app\BaseController ;

use think\cache\driver\Redis;
use think\facade\Db ;
use think\facade\View ;
use think\facade\Request ;
use app\model\Realprice ;

class Index extends BaseController
{
    private $site_title = HEADER_TITLE ;

    private $cachePrefix = 'rpt_';
    /**
     * [index 首頁]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2021-10-02
     */
    public function index( )
    {
        $redis = new Redis() ;
        // $cache_prefix = "main_city" ;
        // if ( !($data = $redis->get( $cache_prefix))) {
        //     $main_city =  Db::table( 'land_main')->distinct(true)->field('tw_city')->select()->toArray() ;
        //     $redis->set( $cache_prefix, $main_city, 3600) ;
        // }

        // dumpa( [SERVER_ADDRESS, DIR_INITIAL]) ;

        // 紀錄查詢關鍵字
	    $this->save_user_operate( ['func' => 'index/index', 'find' => '']) ;
	    
        // 取得所有城市
        $main_city = ext_config('main_city') ;
        // dump( $main_city) ;
        $colBoxLists = [] ;
        $addrMaps = [] ;
        foreach ( $main_city as $iCnt => $city) {
        	// 分配城市落在那一欄位
            $colBoxLists[ $iCnt%5][] = $city ;
	        
            // 定義快取名稱
            $cache_key = $this->cachePrefix."indexPage_".$city ;
            // if ( !($data = $redis->get( $cache_key))) {  // 使用redis
            if ( !($data = ext_config( $cache_key))) {
                $whereArr['tw_city'] = $city ;
                // 之前的方法
                // $whereArr['buy_type'] = '房地(土地+建物)' ;
                // $data = Db::table('land_main_new')->where($whereArr)
                //     ->orderRaw('rand()')->limit(10) // 如果下的查詢有mysql function 要用orderRaw（）處理
                //     ->select()
                //     ->toArray() ;
                $data = Db::table('land_main_new')->field('tw_city_area, address_turn, COUNT(address_turn) cnt')
                    ->where($whereArr)
                    ->where('buy_type', 'in', '房地(土地+建物),房地(土地+建物)+車位')
                    ->group('address_turn')->order('cnt', 'desc')
                    ->limit(100)->select()->toArray() ;
                // $redis->set( $cache_key, $main_city, 3600) ;
                mac_arr2file( DIR_EXTRA.$cache_key.".php", $data) ;

            }
            shuffle( $data) ; // 打散
            $data = array_slice($data, 0, 10); // 再取前十筆
            $addrMaps[ $iCnt%5][] = $data ;
        }

        // dumpa( $colBoxLists) ;

        // Header Meta ---------------------------------------------------
        View::assign( 'home_site',          HTTP_SERVER) ;
        View::assign( 'app_title',          HEADER_TITLE) ;
        // <!-- 主要關鍵字 — 次要關鍵字 | 廠牌名稱 -->
        View::assign( 'title',              HEADER_TITLE) ;
        View::assign( 'meta_description',   '....') ;

        View::assign( 'og_url',             HTTP_SERVER) ;
        View::assign( 'og_site_name',       HEADER_TITLE_SHORT) ;
        View::assign( 'og_title',           HEADER_TITLE_SHORT) ;
        View::assign( 'og_desc',            '....') ;
        View::assign( 'og_updated_time', OG_UPDATED_TIME) ;
        // <!-- This is the URL for this page -->
        // <!--
        // og:title - 網頁標題
        // og:type - 內容的媒體類型，預設為 website
        // og:image - 分享網頁時會顯示的縮圖
        // og:url - 網頁的標準網址，也就是這網頁在社交關係圖中的 ID
        // -->
        // <!--
        // 測試
        // 最後，想要測試或是預覽我們的 OG 標籤有沒有正常運作可以看看下面這些工具：

        // Facebook Debugger
        // Twitter Card Validator
        // Free Open Graph Generator and Preview，這工具除了能看到 Facebook、Twitter、LinkedIn、Discord 上的結果，還可以幫產生 OG 標籤～

        // https://ithelp.ithome.com.tw/m/articles/10278469
        // https://blog.poychang.net/how-to-use-html-head/
        // https://www.minwt.com/webdesign-dev/html/21107.html
        // -->
        // Header Meta End -----------------------------------------------


        View::assign( 'main_city', $main_city) ;
        View::assign( 'addrMaps',  $addrMaps) ;

        View::assign( 'colBoxLists', $colBoxLists) ;

        return View::fetch();
        // return '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ThinkPHP V' . \think\facade\App::version() . '<br/><span style="font-size:30px;">14载初心不改 - 你值得信赖的PHP框架</span></p><span style="font-size:25px;">[ V6.0 版本由 <a href="https://www.yisu.com/" target="yisu">亿速云</a> 独家赞助发布 ]</span></div><script type="text/javascript" src="https://tajs.qq.com/stats?sId=64890268" charset="UTF-8"></script><script type="text/javascript" src="https://e.topthink.com/Public/static/client.js"></script><think id="ee9b1aa918103c4fc"></think>';
    }

    /**
     * [city 第一階 城市]
     * @param   string     $city [description]
     * @param   string     $area [description]
     * @return  [type]           [description]
     * @Another Angus
     * @date    2021-12-12
     */
    public function city( $city = '', $area = '') {
    	// 紀錄查詢關鍵字
	    $this->save_user_operate( ['func' => 'index/city', 'find' => $city.','.$area]) ;
	    
        // 城市不對 就踢去首頁
    	$main_city = ext_config('main_city') ;
        if ( !in_array( $city, $main_city)) {
            header( "Location: ".HTTP_SERVER);
            die();
        }
        $cityIdx = array_search( $city, $main_city) ;

        $cache_key = $this->cachePrefix."city_area_".$cityIdx ;
        $city_areaMaps = ext_config( $cache_key) ;
        if ( $area != '') {
            // 區域不對 就踢去首頁
            if ( !in_array( $area, $city_areaMaps)) {
                header( "Location: ".HTTP_SERVER);
                die();
            }
        }

        // 建立區的地址
        foreach ($city_areaMaps as $aIdx => $aName) {
            $cache_key = $this->cachePrefix."city_area_".$cityIdx."_".$aIdx ;
            if ( !($data = ext_config( $cache_key))) {
                $whereArr['tw_city'] = $city ;
                $whereArr['tw_city_area'] = $aName ;
                $data = Db::table('land_main_new')->field('tw_city_area, address_turn, COUNT(address_turn) cnt')
                    ->where($whereArr)
                    ->where('buy_type', 'in', '房地(土地+建物),房地(土地+建物)+車位')
                    ->group('address_turn')->order('cnt', 'desc')
                    ->limit(100)->select()->toArray() ;
                // $redis->set( $cache_key, $main_city, 3600) ;
                mac_arr2file( DIR_EXTRA.$cache_key.".php", $data) ;
            }
            shuffle( $data) ; // 打散
            $data = array_slice($data, 0, 10); // 再取前十筆
            $area_addr[$aIdx] = $data ;
        }

        // Header Meta ---------------------------------------------------
        if ( !empty($area)) {
            $meta_title = "{$area} - {$city} - " . HEADER_TITLE_SHORT ;
            $meta_url = HTTP_SERVER."{$city}/{$area}" ;
        } else if ( !empty($city)) {
            $meta_title = "{$city} - " .HEADER_TITLE_SHORT ;
            $meta_url = HTTP_SERVER."{$city}" ;
        }

        View::assign( 'home_site',          HTTP_SERVER) ;
        View::assign( 'app_title',          HEADER_TITLE) ;
        // <!-- 主要關鍵字 — 次要關鍵字 | 廠牌名稱 -->
        View::assign( 'title',              $meta_title) ;
        View::assign( 'meta_description',   '....') ;

        View::assign( 'og_url',             $meta_url) ;
        View::assign( 'og_site_name',       HEADER_TITLE_SHORT) ;
        View::assign( 'og_title',           $meta_title) ;
        View::assign( 'og_desc',            '....') ;
        View::assign( 'og_updated_time', OG_UPDATED_TIME) ;
        // Header Meta End -----------------------------------------------
        
        View::assign( 'main_city',  $main_city) ;
        View::assign( 'cityIdx',    $cityIdx) ;
        View::assign( 'areaMaps',   $city_areaMaps) ;
        View::assign( 'area_addr',  $area_addr) ;
        View::assign( 'city_name',  $city) ;

        return View::fetch('index/area_list');
    }

    /**
     * [road 查詢地址]
     * @param   string     $city [description]
     * @param   string     $area [description]
     * @param   string     $road [description]
     * @return  [type]           [description]
     * @Another Angus
     * @date    2021-12-23
     */
    public function road( $city= '', $area = '', $road = '') {
		// 紀錄查詢關鍵字
    	$this->save_user_operate( ['func' => 'index/road', 'find' => $city.','.$area.','.$road]) ;
        $main_city = ext_config('main_city') ;
        // dump( [$city, $area, $road]) ;

        if ( in_array( $city, $main_city)) {
            $whereArr['tw_city'] = $city ;
            $cityKey = array_search( $city, $main_city) ;
        } else {
			header( "Location: ".HTTP_SERVER);
			die();
        }

        $cache_key = $this->cachePrefix.'city_area_'.$cityKey ;
        $areaMaps = ext_config( $cache_key) ;
        if ( !$areaMaps) {
        	$this->save_user_operate( ['func' => 'index/road', 'find' => "[查詢錯誤][{$cache_key}]".$city.','.$area.','.$road]) ;
        }
        if ( in_array( $area, $areaMaps)) {
            $whereArr['tw_city_area'] = $area ;
        } else {
			header( "Location: ".HTTP_SERVER);
			die();
        }

        $whereArr['address_turn'] = $road ;
        $whereArr['buy_type'] = '房地(土地+建物)' ;

        $data = Db::table('land_main_new')->where($whereArr)->order('jiaoyi_date', 'desc')->select()->toArray() ;

        // dump( count($data)) ;

      // Header Meta ---------------------------------------------------
      	if ( !empty($road)) {
            $meta_title = "{$road} - {$area} - {$city} - " . HEADER_TITLE_SHORT ;
            $meta_url = HTTP_SERVER."{$city}/{$area}/{$road}" ;
      	} else if ( !empty($area)) {
            $meta_title = "{$area} - {$city} - " . HEADER_TITLE_SHORT ;
            $meta_url = HTTP_SERVER."{$city}/{$area}" ;
        } else if ( !empty($city)) {
            $meta_title = "{$city} - " .HEADER_TITLE_SHORT ;
            $meta_url = HTTP_SERVER."{$city}" ;
        }

        View::assign( 'home_site',          HTTP_SERVER) ;
        View::assign( 'app_title',          HEADER_TITLE) ;
        // <!-- 主要關鍵字 — 次要關鍵字 | 廠牌名稱 -->
        View::assign( 'title',              $meta_title) ;
        View::assign( 'meta_description',   '....') ;

        View::assign( 'og_url',             $meta_url) ;
        View::assign( 'og_site_name',       HEADER_TITLE_SHORT) ;
        View::assign( 'og_title',           $meta_title) ;
        View::assign( 'og_desc',            '....') ;
        View::assign( 'og_updated_time', OG_UPDATED_TIME) ;
        // Header Meta End -----------------------------------------------

        View::assign( 'home_site',  HTTP_SERVER) ;
        View::assign( 'main_city',  $main_city) ;
        View::assign( 'data',       $data) ;

        return View::fetch('index/road_list');
    }

    /**
     * [search description]
     * @param   string     $road [description]
     * @return  [type]           [description]
     * @Another Angus
     * @date    2022-06-27
     */
    public function search( $road='')
    {	// http://rpt.localhost/search/know4destiny
    	$this->save_user_operate( ['func' => 'index/search', 'find' => $road]) ;
    	dump( $road) ;
    }

    /**
     * [save_user_operate 使用者操作紀錄]
     * @param   array      $filters [description]
     * @return  [type]              [description]
     * @Another Angus
     * @date    2022-06-23
     */
    public function save_user_operate( $filters = []) {
        $info = Request::header() ;
        $full_url = Request::url() ;
	    $baseUrl = Request::baseUrl() ;
	    // dump( [$full_url, $baseUrl ]) ;
	    // dump( [Request::get(), Request::post()]) ;
	    
        // server回傳包含cloudflare提供的表頭
		// "cdn-loop"                  => "cloudflare"
		// "cf-ipcountry"              => "TW"
		// "cf-connecting-ip"          => "122.116.29.168"
		// "accept-language"           => "zh-TW,zh;q=0.9,en;q=0.8"
		// "sec-fetch-dest"            => "document"
		// "sec-fetch-user"            => "?1"
		// "sec-fetch-mode"            => "navigate"
		// "sec-fetch-site"            => "none"
		// "accept"                    => "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"
		// "user-agent"                => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36"
		// "upgrade-insecure-requests" => "1"
		// "sec-ch-ua-platform"        => ""macOS""
		// "sec-ch-ua-mobile"          => "?0"
		// "sec-ch-ua"                 => "" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102""
		// "cf-visitor"                => "{"scheme":"https"}"
		// "x-forwarded-proto"         => "https"
		// "cf-ray"                    => "71f66e160ea87ec0-LAX"
		// "x-forwarded-for"           => "122.116.29.168"
		// "accept-encoding"           => "gzip"
		// "connection"                => "Keep-Alive"
		// "host"                      => "rpt01.com"

        $ip = isset( $info['x-forwarded-for']) ? $info['x-forwarded-for'] : Request::ip() ;
        // $country = $info['HTTP_CF_IPCOUNTRY'] ;
        $data['host']       = $info['host'];
        $data['cf_country'] = isset( $info['cf-ipcountry']) ? $info['cf-ipcountry'] : '' ;
        $data['find_str']   = $filters['find'];
        $data['query_string'] = $full_url ;
        $data['in_func']    = $filters['func'];
        $data['ip_str']     = $ip;
        $data['user_agent'] = $info['user-agent'];
        $data['created']    = date('Y-m-d h:s:i');

        Db::name('find_log')->save( $data) ;
    }

    /**
     * [source 建立城市的區清單]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2022-05-29
     */
    public function source()
    {
        $main_city = ext_config('main_city') ;
        // dump( $main_city) ; exit() ;
        foreach ( $main_city as $iCnt => $city) {
            $ret = Db::table( 'land_main')
                ->where('tw_city', '=', $city)
                ->distinct( true)
                ->field( 'tw_city_area')
                ->select()->toArray();
            $AtoF = [] ;
            foreach ( $ret as $rCnt => $item) {
                if ( $item['tw_city_area'] != '') {
                    $AtoF[] = $item['tw_city_area'] ;
                }
            }
            mac_arr2file( DIR_EXTRA.$this->cachePrefix.'city_area_'.$iCnt.".php", $AtoF) ;
        }

        dump('success') ;
        // return View::fetch();
    }

    public function noUseCode() {
        foreach ( $main as $iCnt => $city) {
            $ret = Db::table( 'land_main')
                ->where('tw_city', '=', $city)
                ->distinct( true)
                ->field( 'tw_city_area')
                ->select()->toArray();
            $AtoF = [] ;
            foreach ( $ret as $rCnt => $item) {
                $AtoF[] = $item['tw_city_area'] ;
            }
            mac_arr2file( DIR_EXTRA.'city_area_'.$iCnt.".php", $AtoF) ;
			// exit() ;
        }
    }

    /**
     * [version 線上主機同步git]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2022-06-27
     */
    public function version( $user = '')  {
    	$this->save_user_operate( ['func' => 'index/version', 'find' => '更新程式']) ;
    	// dump( $user) ;
    	// exit();
    	if ( isset( $user) && $user == 'know4destiny') {
			echo "console command : git branch<br>" ;
			exec('git branch', $result);
			echo "<pre>" ;
			foreach ($result as $iCnt => $line) {
			        echo $line."<br>" ;
			}
			$result = [];
			echo "</pre>" ;


			echo "console command : git pull<br>" ;
			// exec('/usr/bin/sudo /usr/bin/git pull', $result, $return_var);
			exec('git pull', $result);

			echo "<pre>" ;
			foreach ($result as $iCnt => $line) {
			        echo $line."<br>" ;
			}
			echo "</pre>" ;
			echo "<hr>" ;

    	} else {
			header( "Location: ".HTTP_SERVER);
			die();
    	}
    }

    /**
     * [hello description]
     * @param   string     $name [description]
     * @return  [type]           [description]
     * @Another Angus
     * @date    2021-10-02
     */
    public function hello($name = 'ThinkPHP6')
    {
        return 'hello,' . $name;
    }

}