<?php
namespace app\controller;

use app\BaseController ;

use think\cache\driver\Redis;
use think\facade\Db ;
use think\facade\View ;
use think\facade\Request ;
use app\model\Realprice ;

/**

CREATE TABLE `find_log` (
	`idx` BIGINT(20) NOT NULL AUTO_INCREMENT COMMENT '流水號',
	`cf_country` VARCHAR(255) NULL DEFAULT NULL COMMENT '來源國家' COLLATE 'utf8_general_ci',
	`host` VARCHAR(255) NULL DEFAULT NULL COMMENT '主機名稱' COLLATE 'utf8_general_ci',
	`find_str` VARCHAR(255) NULL DEFAULT NULL COMMENT '查詢字串' COLLATE 'utf8_general_ci',
	`in_func` VARCHAR(255) NULL DEFAULT NULL COMMENT '進線func' COLLATE 'utf8_general_ci',
	`ip_str` VARCHAR(255) NULL DEFAULT NULL COMMENT 'IP位址' COLLATE 'utf8_general_ci',
	`user_agent` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_general_ci',
	`created` DATETIME NULL DEFAULT NULL COMMENT '建立時間',
	PRIMARY KEY (`idx`) USING BTREE
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=3
;


 */

class Test extends BaseController
{
	
	public function index() {
		$info = Request::header() ;
		// dump( $info) ;
		
	}
	
	
	public function index_t1( )
    {
    	// https://developers.cloudflare.com/fundamentals/get-started/reference/http-request-headers/
    	// ini_set('memory_limit', '512M') ;

		$info = Request::header() ;
		dump( $info) ;
	    $host = $info['host'] ;
	    $user_agent = $info['user-agent'] ;
	    $ip = Request::ip() ;
	    // $country = $info['HTTP_CF_IPCOUNTRY'] ;
	    $data['host'] = $host;
	    $data['cf_country'] = 'tw';
	    $data['find_str'] = "Angus";
	    $data['in_func'] = "index/city";
	    $data['ip_str'] = $ip;
	    $data['user_agent'] = $user_agent;
	    $data['created'] = date('Y-m-d h:s:i');

	    Db::name('find_log')->save( $data) ;
	
		$whereArr['tw_city'] = '臺北市' ;
		// $whereArr['buy_type'] = '房地(土地+建物)' ;

	    $data = Db::table('land_main_new')->field('tw_city_area, address_turn, COUNT(address_turn) cnt')
	    			->where($whereArr)
	    			->where('buy_type', 'in', '房地(土地+建物),房地(土地+建物)+車位')
	    			->group('address_turn')->order('cnt', 'desc')
	    			->limit(100)->select()->toArray() ;
		dump( $data) ;

	    


    }
}