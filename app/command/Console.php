<?php
declare (strict_types = 1);

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\facade\Log ;

class Console extends Command
{
    protected function configure()
    {
        $this->setName('console')
	        ->addOption('parameter', null, Option::VALUE_REQUIRED, '參數')
	        ->addOption('para2', null, Option::VALUE_REQUIRED, '參數2 以此類推')
            ->addArgument('argv1', Argument::OPTIONAL, "執行的功能名稱")
            ->setDescription('the console command 命令列指令範例');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('console 测试日志信息 請到 [runtime/log/] 確認');
        Log::record('测试日志信息 請到 [runtime/log/] 確認');
        Log::record('测试日志信息，这是警告级别','notice');
        Log::error('错误信息');
        Log::info('日志信息');
        trace('错误信息', 'error');
        trace('日志信息', 'info');
        Log::diy('这是一个自定义日志类型');

		app_path() ;
		base_path() ;
		config_path() ;
		public_path() ;
		runtime_path() ;
		root_path() ;

		// 取得參數作業
	    // 檢查是否有參數	$input->hasOption( '參數名稱') ;
	    // 取出參數值		$input->getOption( '參數名稱') ;
		if ( $input->hasOption( 'parameter'))	{
			var_dump($input->getOption('city1')) ;
		}

        $console_command = 'ps aux | grep "php think"' ;
        $check = shell_exec( $console_command) ;
        // var_dump( $check) ;
        $findProcess = " php think hello" ;
        if ( strpos( $check, $findProcess) !== false) {
            echo "轉檔中\n";
        } else {
            echo "可以進行轉檔\n" ;
        }

    }
}