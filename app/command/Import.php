<?php
declare (strict_types = 1);

namespace app\command;

use PHPExcel_IOFactory;
use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;
use think\Exception;
use think\facade\Log ;
use think\facade\Db ;
use PDOException ;

class Import extends Command
{
	private $rootPath = '' ;
	private $cityNames = [] ;
	private $chineseNumber = [] ;
	private $runIndex = [] ;

    protected function configure()
    {
        // 指令配置
        $this->setName('import')
			->addOption('mod', null, Option::VALUE_REQUIRED, '[land實價,school學校]')
			->addOption('file', null, Option::VALUE_REQUIRED, '[匯入檔案]')
            ->setDescription('the import command');
    }

    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $output->writeln('import');
        $mod = '' ;
		// 取得參數作業
		// 檢查是否有參數	$input->hasOption( '參數名稱') ;
		// 取出參數值		$input->getOption( '參數名稱') ;
		if ( $input->hasOption( 'mod'))	{
			// var_dump($input->getOption('mod')) ;
			$mod = $input->getOption('mod') ;
		}
		switch ( $mod) {
			case 'land': $this->import_land_excel() ; break ;

			default:
				var_dump($input->getOption('mod')) ;
				break ;
		}
        //var_dump( [__DIR__, dirname(__FILE__), $_SERVER['DOCUMENT_ROOT']]) ;
    }

    /**
     * [import_land_excel description]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2021-12-31
     */
	protected function import_land_excel () {
		ini_set("memory_limit","4096M");
		$this->cityNames = ext_config('tw_division') ;
		$this->chineseNumber = ext_config('chinese_number') ;
//		mac_arr2file( DIR_EXTRA."run_excel_index.php", []) ;
//		exit() ;
		$this->runIndex = ext_config( 'run_excel_index') ;

		// $memoryUsage = memory_get_usage();
		// echo "記憶體用量：$memoryUsage 位元組\n";
		// $ret = db('land_main')->limit(10)->select()->toArray() ;

		// test area
//		Log::record("讀取: /www/wwwroot/rpt.com/importData/land/list_a1080101_1121231.xls");
//		echo "/www/wwwroot/rpt.com/importData/land/list_a1080101_1121231.xls\n" ;
//		$this->loadLandExcelFile( ['/www/wwwroot/rpt.com/importData/land/list_a1080101_1121231.xls','a']) ;
//		exit() ;

		$fileLists = $this->loadFileList() ;
		foreach ( $fileLists as $iCnt => $fileSet) {
			if ( !in_array( $fileSet[0], $this->runIndex)) {
				Log::record("讀取: {$fileSet[0]}");
				echo "{$fileSet[0]}\n" ;
				$this->loadLandExcelFile( $fileSet) ;
				$this->runIndex[] = $fileSet[0] ;
				mac_arr2file( DIR_EXTRA."run_excel_index.php", $this->runIndex) ;
			} else {
				echo "已匯入 {$fileSet[0]}\n" ;
			}
			// exit() ;
		}
	}

	/**
	 * [loadLandExcelFile description]
	 * @param   [type]     $fileName [description]
	 * @return  [type]               [description]
	 * @Another Angus
	 * @date    2021-12-31
	 */
	protected function loadLandExcelFile( $fileName) {
		error_reporting(E_ALL);

		$memoryUsage = memory_get_usage()/1024;
		echo "記憶體用量：$memoryUsage 位元組\n";

		$objPHPExcel = PHPExcel_IOFactory::load( $fileName[0]) ;
		$memoryUsage = memory_get_usage()/1024;
		echo "讀取後記憶體用量：$memoryUsage 位元組\n";

		$sheetNames = $objPHPExcel->getSheetNames() ;
		$selSheet = $objPHPExcel->getSheet(2)->toArray(null,true,true,true ) ;
		Log::record("預計執行筆數: ". count( $selSheet));
		echo "預計執行筆數: ". count( $selSheet)."\n" ;
		$colMapLand = $this->col_map_land() ;
		$cityName = $this->cityNames[$fileName[1]] ;
		$twCityName = ['台北市', '台中市', '台南市', '台東縣'] ;

		foreach ( $selSheet as $iCnt => $row) {
			// print_r( [$iCnt, $row]) ;
			if ( $iCnt == 1) continue ;
			$ins = [] ;
			foreach ( $colMapLand as $cellKey => $dbColName) {
				if ( isset( $row[$cellKey])) {
					$ins[$dbColName] = $row[$cellKey] ;
				}
			}
			$ins['tw_city'] = $cityName ;
			$ins['address_turn'] = str_replace( $cityName, '', $ins['address']) ;
			foreach ( $twCityName as $changeName) {
				$ins['address_turn'] = str_replace( $changeName, '', $ins['address_turn']) ;
			}
			$ins['address_turn'] = str_replace( $ins['tw_city_area'], '', $ins['address_turn']) ;
			foreach ( $this->chineseNumber as $key => $cNum) {
				$ins['address_turn'] = str_replace( $cNum, $key, $ins['address_turn']) ;
			}

			try {
				db('land_main_new')->insert($ins) ;
			} catch ( \Throwable $th) {
				$SQLCmd = db()->getLastSql();
				Log::error('错误信息:'.$SQLCmd );
				exit() ;
			}

//			if ( !db('land_main_new')->insert($ins)) {
//				Log::error( db()->getLastSql());
//			}
		}
		$objPHPExcel->disconnectWorksheets();
		unset($objPHPExcel) ;
	}

	/**
	 * [col_map_land 內政部 實價登錄excel 欄位對照表]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2021-12-31
	 */
	protected function col_map_land () {
		return [
			'A' => 'tw_city_area',
			'B' => 'buy_type',
			'C' => 'address',
			'D' => 'land_turn_area',
			'E' => 'land_use_type',
			// 'F' =>
			// 'G' =>
			'H' => 'jiaoyi_date',
			'I' => 'jiaoyi_items',
			'J' => 'transfer_target',
			'K' => 'total_level',
			'L' => 'building_type',
			'M' => 'main_use',
			'N' => 'materials',
			'O' => 'completed_date',
			'P' => 'transfer_total_area',
			'Q' => 'structure_building_bedroom',
			'R' => 'structure_building_apartment',
			'S' => 'structure_building_bathroom',
			'T' => 'structure_building_part',
			'U' => 'committee',
			'V' => 'total_price',
			'W' => 'price_square',
			'X' => 'parking_type',
			'Y' => 'parking_square',
			'Z' => 'parking_price',
			'AA' => 'x_lay',
			'AB' => 'y_lay',
			// 'AC' => N
			'AD' => 'memo',
			'AE' => 'sn',
			'AF' => 'main_building_area',
			'AG' => 'attached_building_area',
			'AH' => 'balcony_area',
			'AI' => 'elevator',
			// 'AJ' =>
		] ;

	}

	/**
	 * [loadFileList description]
	 * @return  [type]     [description]
	 * @Another Angus
	 * @date    2021-12-31
	 */
	protected function loadFileList() {
		app_path() ;
		base_path() ;
		config_path() ;
		public_path() ;
		runtime_path() ;
		root_path() ;
		// print_r( [ base_path(), root_path()]) ;

		$readPath = root_path( "importData/land") ;
		$tmpArr = scandir( $readPath) ;

		// $fileNamePattern = '/(\w)\d{7}_\d{7}_a|(\w)\d{7}_\d{7}.xls/' ;
		$fileNamePattern = '/(\w)\d{7}_\d{7}.xls/' ;
		// $fileNamePattern = '/(\w)\d{7}_\d{7}_a.xls/' ;
		$fileLists = [] ;
		foreach ( $tmpArr as $iCnt => $fileName) {
			if ( $fileName != '.' && $fileName != '..' && $fileName != '.DS_Store'
				&& preg_match( $fileNamePattern, $fileName, $matches)) {
				// print_r( $matches) ;
				$area = isset( $matches[2]) ? $matches[2] : isset( $matches[1]) ? $matches[1] : '' ;
				$fileLists[] = [$readPath.$fileName, $area] ;
			}
		}
		return $fileLists ;
	}
}
