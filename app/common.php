<?php
// 应用公共文件

use think\db\BaseQuery;
use think\facade\Db ;
use think\facade\Config ;

define( 'SERVER_ADDRESS',		$_SERVER['HTTP_HOST']) ;
define( 'HTTP_SERVER',			'//' . SERVER_ADDRESS . '/') ;
define( 'DIR_INITIAL',			'/www/wwwroot/rpt01.com/') ;
define( 'DIR_EXTRA',			DIR_INITIAL.'extra/') ;

define( 'HEADER_TITLE',			'台灣實價登錄查詢 - List of 2022!') ;
define( 'HEADER_TITLE_SHORT',	'台灣實價登錄查詢') ;
define( 'OG_UPDATED_TIME',		'2022-06-22T00:00:00+08:00') ;


/**
 * [db description]
 * @param string $name [description]
 * @return BaseQuery [type]           [description]
 * @Another Angus
 * @date    2021-12-11
 */
function db($name = ''): BaseQuery
{ //tp3的 M()
    return Db::connect('mysql')->name($name) ;
}

/**
 * http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications
 */
if (!function_exists('dumpa')) {
    /** * Debug Helper * * Outputs the given variable(s) with formatting and location * * @access public * @param mixed variables to be output */function dumpa(){ list($callee) = debug_backtrace(); $arguments = func_get_args(); $total_arguments = count($arguments); echo '<fieldset style="background: #fefefe !important; border:2px red solid; padding:5px">'; echo '<legend style="background:lightgrey; padding:5px;">'.$callee['file'].' @ line: '.$callee['line'].'</legend><pre>'; $i = 0; foreach ($arguments as $argument) { echo '<br/><strong>Debug #'.(++$i).' of '.$total_arguments.'</strong>: '; var_dump($argument); } echo "</pre>"; echo "</fieldset>";}
}
/**
 * Outputs the given variables with formatting and location. Huge props
 * out to Phil Sturgeon for this one (http://philsturgeon.co.uk/blog/2010/09/power-dump-php-applications).
 * To use, pass in any number of variables as arguments.
 *
 * @return void
 */
if (!function_exists('dumpb')) {
    function dumpb() {
        list($callee) = debug_backtrace();
        $arguments = func_get_args();
        $total_arguments = count($arguments);

        echo '<fieldset style="background:#fefefe !important; border:2px red solid; padding:5px">' . PHP_EOL .
            '<legend style="background:lightgrey; padding:5px;">' . $callee['file'] . ' @ line: ' . $callee['line'] . '</legend>' . PHP_EOL .
            '<pre>';

        $i = 0;
        foreach ($arguments as $argument) {
            echo '<br/><strong>Debug #' . (++$i) . ' of ' . $total_arguments . '</strong>: ';

            if ( (is_array($argument) || is_object($argument)) && count($argument)) {
                print_r($argument);
            } else {
                var_dump($argument);
            }
        }

        echo '</pre>' . PHP_EOL .
            '</fieldset>' . PHP_EOL;
    }
}

/**
 * [ext_config description]
 * @param   [type]     $f [description]
 * @return  [type]        [description]
 * @Another Angus
 * @date    2021-12-11
 */
function ext_config( $f) {
    $file = DIR_EXTRA.$f.".php" ;
    if(is_file($file)) {
        return include $file ;
    }
    return false ;
}


// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

error_reporting(E_ERROR | E_PARSE );

// 应用公共文件
function mac_return($msg,$code=1,$data=''){
	if(is_array($msg)){
		return json_encode($msg);
	}
	else {
		$rs = ['code' => $code, 'msg' => $msg, 'data'=>'' ];
		if(is_array($data)) $rs['data'] = $data;
		return json_encode($rs);
	}
}
function mac_arr2file($f,$arr='')
{
	if(is_array($arr)){
		$con = var_export($arr,true);
	} else{
		$con = $arr;
	}
	$con = "<?php\nreturn $con;";
	mac_write_file($f, $con);
}
function mac_write_file($f,$c='')
{
	$dir = dirname($f);
	if(!is_dir($dir)){
		mac_mkdirss($dir);
	}
	return @file_put_contents($f, $c);
}
function mac_mkdirss($path,$mode=0777)
{
	if (!is_dir(dirname($path))){
		mac_mkdirss(dirname($path));
	}
	if(!file_exists($path)){
		return mkdir($path,$mode);
	}
	return true;
}
function mac_alert($str)
{
	echo '<script>alert("' .$str. '\t\t");history.go(-1);</script>';
}
function mac_alert_url($str,$url)
{
	echo '<script>alert("' .$str. '\t\t");location.href="' .$url .'";</script>';
}
function mac_jump($url,$sec=0)
{
	echo '<script>setTimeout(function (){location.href="'.$url.'";},'.($sec*1000).');</script><span>暂停'.$sec.'秒后继续  >>>  </span><a href="'.$url.'" >如果您的浏览器没有自动跳转，请点击这里</a><br>';
}
function mac_echo($str)
{
	echo $str.'<br>';
	ob_flush();flush();
}
function mac_day($t,$f='',$c='#FF0000')
{
	if(empty($t)) { return ''; }
	if(is_numeric($t)){
		$t = date('Y-m-d H:i:s',$t);
	}
	$now = date('Y-m-d',time());
	if($f=='color' && strpos(','.$t,$now)>0){
		return '<font color="' .$c. '">' .$t. '</font>';
	}
	return  $t;
}

// CurlPOST数据提交-----------------------------------------
function mac_curl_post($url,$data,$heads=array(),$cookie='')
{
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36');
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLINFO_CONTENT_LENGTH_UPLOAD,strlen($data));
	curl_setopt($ch, CURLOPT_HEADER,0);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);

	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	if(!empty($cookie)){
		curl_setopt($ch, CURLOPT_COOKIE, $cookie);
	}
	if(count($heads)>0){
		curl_setopt ($ch, CURLOPT_HTTPHEADER , $heads );
	}
	$response = @curl_exec($ch);
	if(curl_errno($ch)){//出错则显示错误信息
		//print curl_error($ch);
	}
	curl_close($ch); //关闭curl链接
	return $response;//显示返回信息
}
// CurlPOST数据提交-----------------------------------------
function mac_curl_get($url,$heads=array(),$cookie='')
{
	$ch = @curl_init();
	curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36');

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_HEADER,0);
	curl_setopt($ch, CURLOPT_REFERER, $url);
	curl_setopt($ch, CURLOPT_POST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT_MS, 120000); // 設定最長執行 900 毫秒
	if(!empty($cookie)){
		curl_setopt($ch, CURLOPT_COOKIE, $cookie);
	}
	if(count($heads)>0){
		curl_setopt ($ch, CURLOPT_HTTPHEADER , $heads );
	}
	$response = @curl_exec($ch);
	if(curl_errno($ch)){//出错则显示错误信息
		//print curl_error($ch);die;
		dump( curl_error($ch)) ;
	}
	curl_close($ch); //关闭curl链接
	return $response;//显示返回信息
}
function mac_friend_date($time)
{
	if (!$time)
		return false;
	$fdate = '';
	$d = time() - intval($time);
	$ld = $time - mktime(0, 0, 0, 0, 0, date('Y')); //得出年
	$md = $time - mktime(0, 0, 0, date('m'), 0, date('Y')); //得出月
	$byd = $time - mktime(0, 0, 0, date('m'), date('d') - 2, date('Y')); //前天
	$yd = $time - mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')); //昨天
	$dd = $time - mktime(0, 0, 0, date('m'), date('d'), date('Y')); //今天
	$td = $time - mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')); //明天
	$atd = $time - mktime(0, 0, 0, date('m'), date('d') + 2, date('Y')); //后天
	if ($d == 0) {
		$fdate = '刚刚';
	} else {
		switch ($d) {
			case $d < $atd:
				$fdate = date('Y年m月d日', $time);
				break;
			case $d < $td:
				$fdate = '后天' . date('H:i', $time);
				break;
			case $d < 0:
				$fdate = '明天' . date('H:i', $time);
				break;
			case $d < 60:
				$fdate = $d . '秒前';
				break;
			case $d < 3600:
				$fdate = floor($d / 60) . '分钟前';
				break;
			case $d < $dd:
				$fdate = floor($d / 3600) . '小时前';
				break;
			case $d < $yd:
				$fdate = '昨天' . date('H:i', $time);
				break;
			case $d < $byd:
				$fdate = '前天' . date('H:i', $time);
				break;
			case $d < $md:
				$fdate = date('m月d日 H:i', $time);
				break;
			case $d < $ld:
				$fdate = date('m月d日', $time);
				break;
			default:
				$fdate = date('Y年m月d日', $time);
				break;
		}
	}
	return $fdate;
}
function mac_redirect($url,$obj='')
{
	echo '<script>'.$obj.'location.href="' .$url .'";</script>';
	exit;
}