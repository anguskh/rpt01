<?php
namespace app\model ;

use think\facade\Db ;
use think\Cache\driver\Redis ;

/**
 * Class Realprice
 * @package app\model
 */
class Realprice {
    
    /**
     * [getMainCity description]
     * @return  [type]     [description]
     * @Another Angus
     * @date    2021-12-11
     */
    public function getMainCity() {
    	return db('land_main')->limit(10)->select()->toArray() ;
    }

}