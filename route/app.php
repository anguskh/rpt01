<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 
/** apache 偽靜態設定
<IfModule mod_rewrite.c>
 RewriteEngine on
 RewriteBase /
 RewriteCond %{REQUEST_FILENAME} !-d
 RewriteCond %{REQUEST_FILENAME} !-f
 RewriteRule ^(.*)$ index.php?s=/$1 [QSA,PT,L]
</IfModule> 
 */
// https://rpt01.com/
// http://rpt.localhost/sitemap.xml
use think\facade\Route;

Route::get('think', function () {
    return '123hello,ThinkPHP6!';
});

// Route::get('hello/:name', 'index/hello');
// Route::get('city/:name', 'index/city');

// 測試區
Route::get('test',              'test/index');
// Route::get('',                  'test/index');

// 工作區
Route::get('source',            'index/source');
Route::get('gitpull/:user',     'index/version'); // https://rpt01.com/gitpull/know4destiny
Route::get('search/:road',      'index/search');

// 執行區
Route::get('', 'index');
Route::get(':city/:area/:road', 'index/road');
Route::get(':city/:area',       'index/city');
Route::get(':city',             'index/city');

Route::get('search/:road',      'index/search');


