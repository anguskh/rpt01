<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------
return [
    // 指令定义
    'commands' => [
	    'hello' => 'app\command\Hello',
		'console' => 'app\command\Console',
		'import' => 'app\command\Import',
    ],
];

/* add by Angus 2021.11.05
建立 command line 程式
/www/server/php/73/bin/php think make:command hello(這裡指的是你的入口類別名稱)
[msg]: Command:app\command\hello created successfully.

再來到 config/console.php 加入要執行的入口類別名稱
	'hello' => 'app\command\Hello',
	'Console' => 'app\command\Console',

執行(在config/console.php中 前綴詞[key]有分大小寫)
/www/server/php/73/bin/php think hello
[msg]: app\command\hello

/www/server/php/73/bin/php think Console
[msg]: app\command\console

Ex. 'sync' => 'app\command\Sync'
只要執行 php think sync 就會執行這支檔案
                  ^ 執行的方式 後面空格可以接其他arg
             ^ 這個think是是一個PHP檔案 請不要給小去把它刪掉
        ^ PHP執行路徑 = config('PHP_PATH')

主要參閱 https://www.kancloud.cn/manual/thinkphp6_0/1037651


	    'hello' => 'app\command\Hello',
	    'console' => 'app\command\Console',
 */